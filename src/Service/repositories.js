import axios from 'axios'

const Author_API_URL = 'http://localhost:4200'

class RepoDataService {

    async retrieveAllAuthors() {
        //console.log('executed service')
        const response = await axios.get(`${Author_API_URL}/authors`)
        return response;
    }

    async retrieveAuthor(id) {
        //console.log('executed service')
        const response = await axios.get(`${Author_API_URL}/authors/${id}`);
        return response;
    }

    async deleteAuthor(id) {
        //console.log('executed service')
        const response = await axios.delete(`${Author_API_URL}/authors/${id}`);
        return response;
    }

    async updateAuthor(id, Author) {
        //console.log('executed service')
        const response = await axios.put(`${Author_API_URL}/authors/${id}`, Author);
        return response;
    }

    async createAuthor(Author) {
        //console.log('executed service')
        const response = await axios.post(`${Author_API_URL}/authors/`, Author);
        return response;
    }

}

export default new RepoDataService()
