import React from 'react';
import "./bootstrap.min.css"
import ReactDOM from 'react-dom';
import AuthorQuiz from './AuthorQuiz';
import addAuthorFrom from "./AuthorQuiz/addAuthor"
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Route } from 'react-router-dom';


ReactDOM.render(
  <BrowserRouter>
    <React.Fragment>
      <Route exact path="/" component={AuthorQuiz} />
      <Route path="/add" component={addAuthorFrom} />
    </React.Fragment>
  </BrowserRouter>,
  document.getElementById('root')
);

serviceWorker.unregister();
