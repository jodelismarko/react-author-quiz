import React, { Component } from 'react';
import AuthorQuiz from "./AuthorQuiz";
import { shuffle, sample } from "underscore";
import AuthorDataService from "../Service/repositories"
import "./styles.css"

const authors = [{
    name: '',
    imageUrl: '',
    books: [""]
}];

export default class index extends Component {
    
    state = {
        authores:[],
        turnData: this.getTurnData(authors),
        highlight: ""
    };

    async componentDidMount(){
        const response = await AuthorDataService.retrieveAllAuthors();
        this.setState({ authores : response.data})

        const data = await this.getTurnData(response.data)
        this.setState({turnData : data})
    }

    getTurnData(authors) {
        const allBokks = authors.reduce(function (p, c, i) {
            return p.concat(c.books);
        }, []);
    
        const fourRandomBooks = shuffle(allBokks).slice(0, 4);
        const answer = sample(fourRandomBooks);
    
        return {
            books: fourRandomBooks,
            author: authors.find((author) =>
                author.books.some((title) =>
                    title === answer))
        }
    }

    onAnswerSelected = (answer) => {
        const isCorrect = this.state.turnData.author.books.some((book) => book === answer);
        const highlight = isCorrect ? "correct" : "wrong"
        this.setState({highlight})
    }

      
    render() {
        return (
             <AuthorQuiz {...this.state} onAnswerSelected={this.onAnswerSelected} />
        )
    }
}
