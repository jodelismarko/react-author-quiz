import React, { Component } from 'react';
import AuthorDataService from "../Service/repositories"


export default class addAuthorForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            imageUrl: '',
            books: [],
            bookTemp: ''
        }
    }

    onFieldChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    onFormSubmit = async (event) => {
        const {bookTemp, ...newAuthor} = this.state
        AuthorDataService.createAuthor(newAuthor)
        console.log(newAuthor)
    }

    onAddBook = (event) => {
        event.preventDefault();
        let books = this.state.books.concat(this.state.bookTemp);
        this.setState({
            books,
            bookTemp: ''
        });
    }

    render() {
        return (
            <form className="col-12 offset-1" onSubmit={this.onFormSubmit}>
                <div className="text-input">
                    <label htmlFor="name">Name</label>
                    <input
                        type="text"
                        name="name"
                        value={this.state.name}
                        onChange={this.onFieldChange}
                    />
                </div>
                <div className="text-input">
                    <label htmlFor="imageUrl">Image URL</label>
                    <input
                        type="text"
                        name="imageUrl"
                        value={this.state.imageUrl}
                        onChange={this.onFieldChange}
                    />
                </div>
                <div className="text-input">
                    <label htmlFor="bookTemp">Book</label>
                    {this.state.books.map((book, i) => <p key={i}>{book}</p>)}
                    <input
                        type="text"
                        name="bookTemp"
                        value={this.state.bookTemp}
                        onChange={this.onFieldChange}
                    />
                    <button
                        className="btn btn-info"
                        onClick={this.onAddBook}
                    >+</button>
                </div>

                <button role="submit" className="btn btn-success">Add Author</button>
            </form>);
    }
}

